package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	private int planetX;
	private int planetY;
	private int roverX;
	private int roverY;
	private String direction;
	private List<String> planetObstacles;
	private List<String> encounteredObstacles;
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		roverX = 0;
		roverY = 0;
		direction = "N";
		encounteredObstacles = new ArrayList<>();
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		for (String string : planetObstacles) {
			int obstacleX = Integer.parseInt(string.substring(1, 2));
			int obstacleY = Integer.parseInt(string.substring(3, 4));
			if(obstacleX == x && obstacleY == y)
				return true;
		}
		
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		for (int i = 0; i < commandString.length(); i++) {
			switch (commandString.charAt(i)) {
			case 'r':
				turnRight();
				break;
			case 'l':
				turnLeft();
				break;
			case 'f':
				moveForward();
				break;
			case 'b':
				moveBackward();
				break;
			default:
				break;
			}
		}
		String status = "(" + roverX + "," + roverY + "," + direction + ")";
		for (String string : encounteredObstacles) {
			status += string;
		}
		return status;
	}

	private void goUp() throws MarsRoverException {
		int newY = roverY;
		
		if(roverY == planetY - 1)
			newY = 0;
		else
			newY++;
		
		if(planetContainsObstacleAt(roverX, newY)) {
			if(!checkDuplicate("(" + roverX + "," + newY + ")"))
				encounteredObstacles.add("(" + roverX + "," + newY + ")");
		} else {
			roverY = newY;
		}
	}

	private void goDown() throws MarsRoverException {
		int newY = roverY;

		if(roverY == 0)
			newY = planetY - 1;
		else
			newY--;
		
		if(planetContainsObstacleAt(roverX, newY)) {
			if(!checkDuplicate("(" + roverX + "," + newY + ")"))
				encounteredObstacles.add("(" + roverX + "," + newY + ")");
		} else {
			roverY = newY;
		}
	}
	
	private void goRight() throws MarsRoverException {
		int newX = roverX;
		
		if(roverX == planetX - 1)
			newX = 0;
		else
			newX++;
		
		if(planetContainsObstacleAt(newX, roverY)) {
			if(!checkDuplicate("(" + newX + "," + roverY + ")"))
				encounteredObstacles.add("(" + newX + "," + roverY + ")");
		} else {
			roverX = newX;
		}
	}
	
	private void goLeft() throws MarsRoverException {
		int newX = roverX;
		
		if(roverX == 0)
			newX = planetX - 1;
		else
			newX--;
		
		if(planetContainsObstacleAt(newX, roverY)) {
			if(!checkDuplicate("(" + newX + "," + roverY + ")"))
				encounteredObstacles.add("(" + newX + "," + roverY + ")");
		} else {
			roverX = newX;
		}
	}
	
	private void moveForward() throws MarsRoverException {
		switch (direction) {
		case "N":
			goUp();
			break;
		case "E":
			goRight();
			break;
		case "S":
			goDown();
			break;
		case "W":
			goLeft();
			break;
		default:
			break;
		}
	}
	
	private void moveBackward() throws MarsRoverException {
		switch (direction) {
		case "N":
			goDown();
			break;
		case "E":
			goLeft();
			break;
		case "S":
			goUp();
			break;
		case "W":
			goRight();
			break;
		default:
			break;
		}
	}

	private void turnRight() {
		switch (direction) {
		case "N":
			direction = "E";
			break;
		case "E":
			direction = "S";
			break;
		case "S":
			direction = "W";
			break;
		case "W":
			direction = "N";
			break;
		default:
			break;
		}
	}
	
	private void turnLeft() {
		switch (direction) {
		case "N":
			direction = "W";
			break;
		case "W":
			direction = "S";
			break;
		case "S":
			direction = "E";
			break;
		case "E":
			direction = "N";
			break;
		default:
			break;
		}
	}

	private boolean checkDuplicate(String newObstacle) {
		for (String obstacle : encounteredObstacles) {
			if(obstacle.equals(newObstacle)) {
				return true;
			}
		}
		return false;
	}
}
