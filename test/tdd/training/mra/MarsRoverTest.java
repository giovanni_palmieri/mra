package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testContainsObstacleAt() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(rover.planetContainsObstacleAt(4, 7));
	}
	
	@Test
	public void testLandingStatus() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,N)", rover.executeCommand(""));
	}
	
	@Test
	public void testTurningRight() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,E)", rover.executeCommand("r"));
	}
	
	@Test
	public void testTurningLeft() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,W)", rover.executeCommand("l"));
	}
	
	@Test
	public void testMoveForward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,1,N)", rover.executeCommand("f"));
	}
	
	@Test
	public void testMoveBackward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.executeCommand("r");
		rover.executeCommand("f");
		rover.executeCommand("f");
		
		assertEquals("(1,0,E)", rover.executeCommand("b"));
	}
	
	@Test
	public void testCombinedMovement() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(2,2,E)", rover.executeCommand("ffrff"));
	}
	
	@Test
	public void testYCoordinateWrapping() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,9,N)", rover.executeCommand("b"));
	}
	
	@Test
	public void testXCoordinateWrapping() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(9,0,W)", rover.executeCommand("lf"));
	}
	
	@Test
	public void testSingleObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(1,2,E)(2,2)", rover.executeCommand("ffrfff"));
	}
	
	@Test
	public void testMultipleObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(1,1,E)(2,2)(2,1)", rover.executeCommand("ffrfffrflf"));
	}
	
	@Test
	public void testMultipleObstacleWithWrapping() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,9)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,N)(0,9)", rover.executeCommand("b"));
	}
	
	@Test
	public void testPlanetTour() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
		
		MarsRover rover = new MarsRover(6, 6, planetObstacles);
		
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", rover.executeCommand("ffrfffrbbblllfrfrbbl"));
	}
}
